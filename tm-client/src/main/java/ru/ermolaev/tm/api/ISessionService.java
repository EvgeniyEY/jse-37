package ru.ermolaev.tm.api;

import org.jetbrains.annotations.Nullable;
import ru.ermolaev.tm.endpoint.SessionDTO;

public interface ISessionService {

    @Nullable
    SessionDTO getCurrentSession();

    void setCurrentSession(SessionDTO currentSession);

    void clearCurrentSession();

}
