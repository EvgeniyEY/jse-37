package ru.ermolaev.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.ermolaev.tm.api.ISessionService;
import ru.ermolaev.tm.listener.AbstractListener;
import ru.ermolaev.tm.endpoint.ProjectEndpoint;

@Component
public abstract class AbstractProjectListener extends AbstractListener {

    protected ProjectEndpoint projectEndpoint;

    @Autowired
    public AbstractProjectListener(
            @NotNull final ProjectEndpoint projectEndpoint,
            @NotNull final ISessionService sessionService
    ) {
        super(sessionService);
        this.projectEndpoint = projectEndpoint;
    }

}
