package ru.ermolaev.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.ermolaev.tm.api.ISessionService;
import ru.ermolaev.tm.listener.AbstractListener;
import ru.ermolaev.tm.endpoint.TaskEndpoint;

@Component
public abstract class AbstractTaskListener extends AbstractListener {

    protected TaskEndpoint taskEndpoint;

    @Autowired
    public AbstractTaskListener(
            @NotNull final TaskEndpoint taskEndpoint,
            @NotNull final ISessionService sessionService
    ) {
        super(sessionService);
        this.taskEndpoint = taskEndpoint;
    }

}
