package ru.ermolaev.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.ermolaev.tm.api.ISessionService;
import ru.ermolaev.tm.listener.AbstractListener;
import ru.ermolaev.tm.endpoint.UserEndpoint;

@Component
public abstract class AbstractUserListener extends AbstractListener {

    protected UserEndpoint userEndpoint;

    @Autowired
    public AbstractUserListener(
            @NotNull final UserEndpoint userEndpoint,
            @NotNull final ISessionService sessionService
    ) {
        super(sessionService);
        this.userEndpoint = userEndpoint;
    }

}
