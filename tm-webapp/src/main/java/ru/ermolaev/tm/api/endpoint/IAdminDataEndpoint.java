package ru.ermolaev.tm.api.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.ermolaev.tm.dto.SessionDTO;

public interface IAdminDataEndpoint {

    void saveXmlByJaxb(@Nullable SessionDTO sessionDTO) throws Exception;

    void loadXmlByJaxb(@Nullable SessionDTO sessionDTO) throws Exception;

    void clearXmlFileJaxb(@Nullable SessionDTO sessionDTO) throws Exception;

    void saveXmlByFasterXml(@Nullable SessionDTO sessionDTO) throws Exception;

    void loadXmlByFasterXml(@Nullable SessionDTO sessionDTO) throws Exception;

    void clearXmlFileFasterXml(@Nullable SessionDTO sessionDTO) throws Exception;

    void saveJsonByJaxb(@Nullable SessionDTO sessionDTO) throws Exception;

    void loadJsonByJaxb(@Nullable SessionDTO sessionDTO) throws Exception;

    void clearJsonFileJaxb(@Nullable SessionDTO sessionDTO) throws Exception;

    void saveJsonByFasterXml(@Nullable SessionDTO sessionDTO) throws Exception;

    void loadJsonByFasterXml(@Nullable SessionDTO sessionDTO) throws Exception;

    void clearJsonFileFasterXml(@Nullable SessionDTO sessionDTO) throws Exception;

    void saveBinary(@Nullable SessionDTO sessionDTO) throws Exception;

    void loadBinary(@Nullable SessionDTO sessionDTO) throws Exception;

    void clearBinaryFile(@Nullable SessionDTO sessionDTO) throws Exception;

    void saveBase64(@Nullable SessionDTO sessionDTO) throws Exception;

    void loadBase64(@Nullable SessionDTO sessionDTO) throws Exception;

    void clearBase64File(@Nullable SessionDTO sessionDTO) throws Exception;

}
