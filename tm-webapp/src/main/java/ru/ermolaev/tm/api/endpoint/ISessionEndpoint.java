package ru.ermolaev.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ermolaev.tm.dto.Result;
import ru.ermolaev.tm.dto.SessionDTO;

public interface ISessionEndpoint {

    @Nullable
    SessionDTO openSession(@Nullable String login, @Nullable String password) throws Exception;

    @NotNull
    Result closeSession(@Nullable SessionDTO sessionDTO) throws Exception;

    @NotNull
    Result closeAllSessionsForUser(@Nullable SessionDTO sessionDTO) throws Exception;

    @Nullable
    String getServerHost(@Nullable SessionDTO sessionDTO) throws Exception;

    @Nullable
    Integer getServerPort(@Nullable SessionDTO sessionDTO) throws Exception;

}
