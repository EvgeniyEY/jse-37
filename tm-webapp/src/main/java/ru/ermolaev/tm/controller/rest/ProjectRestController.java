package ru.ermolaev.tm.controller.rest;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import ru.ermolaev.tm.api.service.IProjectService;
import ru.ermolaev.tm.dto.ProjectDTO;

import java.util.List;

@RestController
@RequestMapping(value = "/rest/project")
public class ProjectRestController {

    private final IProjectService projectService;

    @Autowired
    public ProjectRestController(
            @NotNull final IProjectService projectService
    ) {
        this.projectService = projectService;
    }

    @PostMapping(value = "/create")
    public void createProject(
            @RequestBody @Nullable final ProjectDTO projectDTO
    ) throws Exception {
        projectService.createProject(
                projectDTO.getUserId(),
                projectDTO.getName(),
                projectDTO.getDescription());
    }

    @PutMapping(value = "/updateById")
    public void updateById(
            @RequestBody @Nullable final ProjectDTO projectDTO
    ) throws Exception {
        projectService.updateById(
                projectDTO.getUserId(),
                projectDTO.getId(),
                projectDTO.getName(),
                projectDTO.getDescription());
    }

    @NotNull
    @GetMapping(value = "/countAll", produces = MediaType.APPLICATION_JSON_VALUE)
    public Long countAllProjects() {
        return projectService.countAllProjects();
    }

    @Nullable
    @GetMapping(value = "/findById/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ProjectDTO findById(
            @PathVariable("id") @Nullable final String id
    ) throws Exception {
        return ProjectDTO.toDTO(projectService.findById(id));
    }

    @NotNull
    @GetMapping(value = "/findAll", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<ProjectDTO> findAll() {
        return projectService.findAll();
    }

    @DeleteMapping(value = "/removeById/{id}")
    public void removeById(
            @PathVariable("id") @Nullable final String id
    ) throws Exception {
        projectService.removeOneById(id);
    }

}
