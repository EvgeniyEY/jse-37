package ru.ermolaev.tm.controller.rest;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import ru.ermolaev.tm.api.service.ITaskService;
import ru.ermolaev.tm.dto.TaskDTO;

import java.util.List;

@RestController
@RequestMapping(value = "/rest/task")
public class TaskRestController {

    private final ITaskService taskService;

    @Autowired
    public TaskRestController(
            @NotNull final ITaskService taskService
    ) {
        this.taskService = taskService;
    }

    @PostMapping(value = "/create")
    public void createTask(
            @RequestBody @Nullable final TaskDTO taskDTO
    ) throws Exception {
        taskService.createTask(
                taskDTO.getUserId(),
                taskDTO.getName(),
                taskDTO.getProjectId(),
                taskDTO.getDescription());
    }

    @PutMapping(value = "/updateById")
    public void updateById(
            @RequestBody @Nullable final TaskDTO taskDTO
    ) throws Exception {
        taskService.updateById(
                taskDTO.getUserId(),
                taskDTO.getId(),
                taskDTO.getName(),
                taskDTO.getDescription());
    }

    @NotNull
    @GetMapping(value = "/countAll", produces = MediaType.APPLICATION_JSON_VALUE)
    public Long countAllTasks() {
        return taskService.countAllTasks();
    }

    @Nullable
    @GetMapping(value = "/findById/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public TaskDTO findById(
            @PathVariable("id") @Nullable final String id
    ) throws Exception {
        return TaskDTO.toDTO(taskService.findById(id));
    }

    @NotNull
    @GetMapping(value = "/findAll", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<TaskDTO> findAll() {
        return taskService.findAll();
    }

    @DeleteMapping(value = "/removeById/{id}")
    public void removeOneById(
            @PathVariable("id") @Nullable final String id
    ) throws Exception {
        taskService.removeOneById(id);
    }

}
