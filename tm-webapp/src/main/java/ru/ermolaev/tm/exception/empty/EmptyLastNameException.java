package ru.ermolaev.tm.exception.empty;

import ru.ermolaev.tm.exception.AbstractException;

public final class EmptyLastNameException extends AbstractException {

    public EmptyLastNameException() {
        super("Error! Last name is empty.");
    }

}
