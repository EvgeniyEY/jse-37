package ru.ermolaev.tm.exception.incorrect;

import org.jetbrains.annotations.NotNull;
import ru.ermolaev.tm.exception.AbstractException;

import java.util.Date;

public final class IncorrectStartDateException extends AbstractException {

    public IncorrectStartDateException() {
        super("Error! Start date is incorrect.");
    }

    public IncorrectStartDateException(@NotNull final Date date) {
        super("Error! This date [" + date + "] is incorrect.");
    }

}
