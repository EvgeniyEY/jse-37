package ru.ermolaev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ermolaev.tm.entity.Task;

import java.util.List;

public interface ITaskRepository extends IRepository<Task> {

    long countByUserId(@NotNull String userId);

    long countByProjectId(@NotNull String projectId);

    long countByUserIdAndProjectId(@NotNull String userId, @NotNull String projectId);

    @Nullable
    Task findByUserIdAndId(@NotNull String userId, @NotNull String id);

    @Nullable
    Task findByUserIdAndName(@NotNull String userId, @NotNull String name);

    @NotNull
    List<Task> findAllByUserId(@NotNull String userId);

    @NotNull
    List<Task> findAllByProjectId(@NotNull String projectId);

    @NotNull
    List<Task> findAllByUserIdAndProjectId(@NotNull String userId, @NotNull String projectId);

    void deleteByUserIdAndId(@NotNull String userId, @NotNull String id);

    void deleteByUserIdAndName(@NotNull String userId, @NotNull String name);

    void deleteAllByUserId(@NotNull String userId);

    void deleteAllByProjectId(@NotNull String projectId);

    void deleteAllByUserIdAndProjectId(@NotNull String userId, @NotNull String projectId);

}
